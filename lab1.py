import numpy as np
from scipy import sparse

# utwórz tablicę zawierającą 10 zer
print('utwórz tablicę zawierającą 10 zer')
arr10 = np.zeros((10,), dtype=int)
print(arr10)

# utwórz tablicę zawierającą 10 piątek,
print('\nutwórz tablicę zawierającą 10 piątek')
arr5 = np.random.randint(5, 6, 10)
print(arr5)
# utwórz tablicę zawierającą liczby od 10 do 50,
print('\nutwórz tablicę zawierającą liczby od 10 do 50')
arr10_50 = np.random.randint(10, 51, 5)
print(arr10_50)
# utwórz macierz (tablica wielowymiarowa) o wymiarach 3x3 zawierającą liczby od 0 do 8,
print('\nutwórz macierz (tablica wielowymiarowa) o wymiarach 3x3 zawierającą liczby od 0 do 8')
arr3x3 = np.random.randint(0, 9, [3, 3])
arr3x3 = np.mat(arr3x3)
print(arr3x3)
# utwórz macierz jednostkową o wymiarach 3x3,
print('\nutwórz macierz jednostkową o wymiarach 3x3')
arr3x3_i = np.zeros((3, 3), dtype=int)
for i in range(3):
    for j in range(3):
        if i == j:
            arr3x3_i[i, j] = 1
arr3x3_i = np.mat(arr3x3_i)
print(arr3x3_i)
# utwórz macierz o wymiarach 5x5 zawierającą liczby z dystrybucji normalnej (Gaussa),
print('\nutwórz macierz o wymiarach 5x5 zawierającą liczby z dystrybucji normalnej (Gaussa)')
arr5x5 = np.array(
    [
        [1, 4, 7, 4, 1],
        [4, 16, 26, 16, 4],
        [7, 26, 41, 26, 7],
        [4, 16, 26, 16, 4],
        [1, 4, 7, 4, 1]
    ]
)
arr5x5 = np.mat(arr5x5)
print(arr5x5)
# utwórz macierz o wymiarach 10x10 zawierającą liczby od 0,01 do 1 z krokiem 0,01,
print('\nutwórz macierz o wymiarach 10x10 zawierającą liczby od 0,01 do 1 z krokiem 0,01')
vals = np.arange(0.01, 1, 0.1)
arr10x10 = np.random.choice(vals, (10, 10))
arr10x10 = np.mat(arr10x10)
print(arr10x10)
# utwórz tablicę zawierającą 20 liniowo rozłożonych liczb między 0 a 1 (włącznie z 0 i 1),
print('\nutwórz tablicę zawierającą 20 liniowo rozłożonych liczb między 0 a 1 (włącznie z 0 i 1)')
arr20 = np.zeros(20)
for x in range(20):
    arr20[x] = x*0.0526315789474
arr20 = np.mat(arr20)
print(arr20)
# utwórz tablicę zawierającą losowe liczby z przedziału (1, 25), następnie zamień ją na macierz o wymiarach 5 x 5 z tymi samymi liczbami:
print('\nutwórz tablicę zawierającą losowe liczby z przedziału (1, 25), następnie zamień ją na macierz o wymiarach 5 x 5 z tymi samymi liczbami')
list25 = np.random.randint(1, 26, 25)
list25 = list25.reshape(5, 5)
list25 = np.mat(list25)
print(list25)
# oblicz sumę wszystkich liczb w ww. macierzy,
print('\noblicz sumę wszystkich liczb w ww. macierzy')
print(list25.sum())
# oblicz średnią wszystkich liczb w ww. macierzy,
print('\noblicz średnią wszystkich liczb w ww. macierzy')
print(list25.mean())
# oblicz standardową dewiację dla liczb w ww. macierzy,
print('\noblicz standardową dewiację dla liczb w ww. macierzy')
print(list25.std())
# oblicz sumę każdej kolumny ww. macierzy i zapisz ją do tablicy.
print('\noblicz sumę każdej kolumny ww. macierzy i zapisz ją do tablicy')
column_sums = list25.sum(axis=0)
print(column_sums)
# utwórz macierz o wymiarach 5x5 zawierającą losowe liczby z przedziału (0, 100) i:
# oblicz medianę tych liczb,
# znajdź najmniejszą liczbę tej macierzy,
# znajdź największą liczbę tej macierzy.
print('\nutwórz macierz o wymiarach 5x5 zawierającą losowe liczby z przedziału (0, 100) i:\noblicz medianę tych liczb,\nznajdź najmniejszą liczbę tej macierzy,\nznajdź największą liczbę tej macierzy.')
arr55 = np.random.randint(0, 101, [5, 5])
matrix5 = np.mat(arr55)
print('\nmacierz')
print(matrix5)
print('\noblicz medianę tych liczb')
print(np.median(arr55))
print('\nznajdź najmniejszą liczbę tej macierzy,')
print(matrix5.min())
print('\nznajdź największą liczbę tej macierzy.')
print(matrix5.max())
# utwórz macierz o wymiarach różnych od siebie i większych od 1, zawierającą losowe liczby z przedziału (0, 100) i dokonaj jej transpozycji,
print('\nutwórz macierz o wymiarach różnych od siebie i większych od 1, zawierającą losowe liczby z przedziału (0, 100) i dokonaj jej transpozycji')
trans_matrix = np.random.randint(0, 101, [3, 2])
trans_matrix = np.mat(trans_matrix)
print(trans_matrix.transpose())
# utwórz dwie macierze o odpowiednich wymiarach (doczytać), większych od 2 i dodaj je do siebie,
print('\nutwórz dwie macierze o odpowiednich wymiarach (doczytać), większych od 2 i dodaj je do siebie')
a = np.random.randint(5, 11, [3, 3], dtype=int)
a = np.mat(a)
b = np.random.randint(5, 11, [3, 3], dtype=int)
b = np.mat(b)
result = np.zeros((3, 3), dtype=int)
result = np.mat(result)

for i in range(len(a)):
    for j in range(len(a[0])):
        result[i][j] = a[i][j] + b[i][j]
print(result)
# utwórz dwie macierze o odpowiednich wymiarach (doczytać) różnych od siebie i większych od 2, a następnie pomnóż je przez siebie za pomocą dwóch różnych funkcji (np. ‘matmul’ i ‘multiply’),
print('\nutwórz dwie macierze o odpowiednich wymiarach (doczytać) różnych od siebie i większych od 2, a następnie pomnóż je przez siebie za pomocą dwóch różnych funkcji (np. ‘matmul’ i ‘multiply’)\nmatmul:')
x = np.random.randint(5, 11, [3, 2], dtype=int)
x = np.mat(x)
y = np.random.randint(5, 11, [2, 3], dtype=int)
y = np.mat(y)
z = np.random.randint(5, 11, [3, 2], dtype=int)
z = np.mat(z)
print(np.matmul(x, y))
print('\n multiply:')
print(np.multiply(x, z))
print('\n dot:')
print(np.dot(x,y))